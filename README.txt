This module extends Views built in handlers. It provides
the ability to output a list of entities linked to their content
except when directly accessing the entity.
