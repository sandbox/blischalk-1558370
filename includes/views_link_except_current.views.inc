<?php
/**
 * @file
 * Provide views data and handlers for node.module
 */

/**
 * Implements hook_views_data()
 */
function views_link_except_current_views_data_alter(&$data) {
  foreach ($data as $table => $config) {
    foreach ($config as $item => $item_config) {
      if (isset($item_config['field']['handler']) && $item_config['field']['handler'] == 'views_handler_field_node') {
        $data[$table][$item]['field']['handler'] = 'views_link_except_current_handler_field_node';
      }
    }
  }
  return $data;
}
